public class phone {
    private final String quality;
    private final int year;
    private final double price;
    private final String manufacturer;

    public phone(String engine, int year, double price, String manufacturer) {
        this.quality = engine;
        this.year = year;
        this.price = price;
        this.manufacturer = manufacturer;
    }

    public String getQuality() {
        return quality;
    }

    public int getYear() {
        return year;
    }

    public double getPrice() {
        return price;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    @Override
    public String toString() {
        return "phone{" +
                "quality='" + quality + '\'' +
                ", year=" + year +
                ", price=" + price +
                ", manufacturer='" + manufacturer + '\'' +
                '}';
    }
}
